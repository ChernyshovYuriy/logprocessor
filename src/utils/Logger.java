package utils;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 9/8/13
 * Time: 10:05 AM
 */
public class Logger {

    public static void printMessage(String message) {
        System.out.println("[I] " + message);
    }

    public static void printWarning(String message) {
        System.out.println("[W] " + message);
    }

    public static void printError(String message) {
        System.out.println("[E] " + message);
    }

	public static void printError(Throwable e) {
		System.out.println("[E] " + e.toString());
	}
}