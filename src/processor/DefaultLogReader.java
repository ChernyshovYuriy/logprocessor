package processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: demon
 * Date: 08.09.13
 * Time: 19:19
 */
public class DefaultLogReader implements LogReader {

    private LogReaderListener logReaderListener;

    public DefaultLogReader(LogReaderListener listener) {
        logReaderListener = listener;
    }

    @Override
    public void processLog(InputStreamReader reader) throws IOException {
        BufferedReader bufferedInputStream = new BufferedReader(reader);
        String line;
        while ((line = bufferedInputStream.readLine()) != null) {
            LogEntity logEntity = parseLogLine(line);
            logReaderListener.processLogRecord(logEntity);
        }
        logReaderListener.onProcessComplete();
    }

    public LogEntity parseLogLine(String logLine) {
        LogEntity logEntity = new LogEntity();
        int firstWhitespaceIndex = logLine.indexOf(" ");
        int secondWhitespaceIndex = logLine.indexOf(" ", firstWhitespaceIndex + 1);
        String time = logLine.substring(0, logLine.indexOf(" "));
        String severity = logLine.substring(firstWhitespaceIndex + 1, secondWhitespaceIndex - 1);
        String message = logLine.substring(secondWhitespaceIndex, logLine.length());
        logEntity.setTime(time);
        logEntity.setSeverity(severity.trim());
        logEntity.setMessage(message.trim());
        return logEntity;
    }
}