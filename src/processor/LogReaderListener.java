package processor;

/**
 * Created with IntelliJ IDEA.
 * User: demon
 * Date: 08.09.13
 * Time: 19:21
 */
public interface LogReaderListener {
    public void processLogRecord(LogEntity record);
    public void onProcessComplete();
}