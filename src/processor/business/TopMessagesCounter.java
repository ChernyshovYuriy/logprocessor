package processor.business;

import processor.LogEntity;
import processor.LogReaderListener;
import utils.Logger;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: demon
 * Date: 08.09.13
 * Time: 19:40
 */
public class TopMessagesCounter implements LogReaderListener {

    Hashtable<String, Hashtable<String, Integer>> resultHolderTable;

    public TopMessagesCounter() {
        resultHolderTable = new Hashtable<String, Hashtable<String, Integer>>();
    }

    @Override
	public void processLogRecord(LogEntity record) {
        Hashtable<String, Integer> messagesCounterTable = resultHolderTable.get(record.getSeverity());
        if (messagesCounterTable == null) {
            messagesCounterTable = new Hashtable<String, Integer>();
        }
        Integer counter = messagesCounterTable.get(record.getMessage());
        if (counter == null) {
            counter = 0;
        }
        counter++;
        messagesCounterTable.put(record.getMessage(), counter);
        resultHolderTable.put(record.getSeverity(), messagesCounterTable);
	}

    @Override
    public void onProcessComplete() {
        Set<String> keys = resultHolderTable.keySet();
        int displayResultCounter;
        for (String key: keys) {
            Logger.printMessage("");
            Logger.printMessage("Severity: " + key);
            Logger.printMessage("   top messages are:");
            Hashtable<String, Integer> messagesCounterTable = resultHolderTable.get(key);

            // Sort a results
            ArrayList<Map.Entry<String, Integer>> arrayList =
                    new ArrayList<Map.Entry<String, Integer>>(messagesCounterTable.entrySet());
            Collections.sort(arrayList, new Comparator<Map.Entry<String, Integer>>() {
                @Override
                public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                    return o2.getValue().compareTo(o1.getValue());
                }
            });

            displayResultCounter = 0;
            for (Map.Entry<String, Integer> entries : arrayList) {
                Logger.printMessage("      message: '" + entries.getKey() + "', number of entries: " + entries.getValue());
                if (displayResultCounter++ >= 2) {
                    break;
                }
            }
        }
    }
}