package processor.business;

import processor.LogEntity;
import processor.LogReaderListener;
import utils.Logger;

import java.util.Hashtable;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: demon
 * Date: 08.09.13
 * Time: 19:27
 */
public class SeverityMessagesCounter implements LogReaderListener {

    private Hashtable<String, Integer> resultHolderTable;

    public SeverityMessagesCounter() {
        resultHolderTable = new Hashtable<String, Integer>();
    }

    @Override
	public void processLogRecord(LogEntity record) {
        Integer counter = resultHolderTable.get(record.getSeverity());
        if (counter == null) {
            counter = 0;
        }
        counter++;
        resultHolderTable.put(record.getSeverity(), counter);
	}

    @Override
    public void onProcessComplete() {
        Set<String> keys = resultHolderTable.keySet();
        for (String key: keys) {
            Logger.printMessage("Severity: " + key + ", messages number: " + resultHolderTable.get(key));
        }
    }
}