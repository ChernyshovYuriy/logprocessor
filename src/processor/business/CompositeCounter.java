package processor.business;

import processor.LogEntity;
import processor.LogReaderListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: demon
 * Date: 08.09.13
 * Time: 19:42
 */
public class CompositeCounter implements LogReaderListener {

    List<LogReaderListener> logReaderListeners = new ArrayList<LogReaderListener>();

    public CompositeCounter(LogReaderListener... listeners) {
        for (LogReaderListener listener : listeners) {
            this.logReaderListeners.add(listener);
        }
    }

    @Override
    public void processLogRecord(LogEntity record) {
        for (LogReaderListener listener : logReaderListeners) {
            listener.processLogRecord(record);
        }
    }

    @Override
    public void onProcessComplete() {
        for (LogReaderListener listener : logReaderListeners) {
            listener.onProcessComplete();
        }
    }
}
