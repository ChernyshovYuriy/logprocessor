package processor;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 9/8/13
 * Time: 11:19 AM
 */
public class LogEntity {

    private String time;
    private String severity;
    private String message;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "LogEntity - time: " + getTime() + ", severity: " + getSeverity() + ", message: " + getMessage();
    }
}