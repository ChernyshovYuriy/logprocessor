package processor;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: demon
 * Date: 08.09.13
 * Time: 19:19
 */
public interface LogReader {
    public void processLog(InputStreamReader reader) throws IOException;
}