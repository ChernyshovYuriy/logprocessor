import processor.*;
import processor.business.CompositeCounter;
import processor.business.SeverityMessagesCounter;
import processor.business.TopMessagesCounter;
import utils.Logger;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        doProcess();
    }

    private static void doProcess() {

        InputStream inputStream = null;
        File file = new File("assets/log.txt");

        try {
            inputStream = new FileInputStream(file);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

            SeverityMessagesCounter messageCounter = new SeverityMessagesCounter();
            TopMessagesCounter topCounter = new TopMessagesCounter();

            LogReaderListener compositeListener = new CompositeCounter(messageCounter, topCounter);
            LogReader logReader = new DefaultLogReader(compositeListener);

            logReader.processLog(inputStreamReader);

        } catch (IOException e) {
            Logger.printError("Can not processLog Log: " + e.getMessage());
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Logger.printError(e.toString());
                }
            }
        }
    }
}